<?php

namespace AppBundle\Components;

class Escaper
{
    protected $connection;

    public function __construct($connection)
    {
        $this->connection = $connection;
    }

    /**
     * @return \Doctrine\DBAL\Connection
     */
    public function getConnection()
    {
        return $this->connection;
    }

    /**
     * Clear string from injection
     * @return string
     */
    public function clear($string)
    {
        return $this->connection->quote(htmlspecialchars(strip_tags($string)));
    }
}