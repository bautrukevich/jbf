<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use AppBundle\Entity\SimpleUser;
use AppBundle\Entity\SimpleGroup;

use AppBundle\Components\Escaper;

class SimpleUsersController extends Controller
{
    /**
     * @Route("/users/add/{username}/{groupname}", name="add_user_with_group")
     */
    public function createUserWithGroupAction($username, $groupname)
    {
        $username = (new Escaper($this->get('database_connection')))->clear($username);
        $groupname = (new Escaper($this->get('database_connection')))->clear($groupname);

        $isUserExists = $this->getDoctrine()
            ->getRepository('AppBundle:SimpleUser')
            ->findOneByName($username);
        $isGroupExists = $this->getDoctrine()
            ->getRepository('AppBundle:SimpleGroup')
            ->findOneByName($groupname);

        if ($isUserExists) {
            $user = $isUserExists;
        } else {
            $user = new SimpleUser();
            $user->setName($username);
        }

        if ($isGroupExists) {
            $group = $isGroupExists;
        } else {
            $group = new SimpleGroup();
            $group->setName($groupname);
        }

        $isUserContainsGroup = $user->getGroups()->contains($group);
        if ($isUserContainsGroup) {
            return new Response(
                'User with name «'.$user->getName().'» has a group with name «'.$group->getName().'»'
            );
        } else {
            // add this user to the group
            $user->addGroup($group);
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($group);
        $em->persist($user);
        $em->flush();

        return new Response(
            'Saved user with name «'.$user->getName()
            .'» and group with name «'.$group->getName().'»'
        );
    }
}
